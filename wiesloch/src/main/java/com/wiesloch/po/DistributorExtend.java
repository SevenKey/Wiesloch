package com.wiesloch.po;

/**
 * 
 * ClassName:DistributorExtend<br/>
 * Description:分销商的扩展类 可以加入一些其他的字段信息来扩展我们的分销商类<br/>
 * E-mail:weijianyututu@163.com <br/>
 * Date: 2016年4月29日 下午4:10:07 <br/>
 * 
 * @author SevenKey
 * @version 1.0
 * @since JDK 1.7
 *
 */
public class DistributorExtend extends Distributor {

}
