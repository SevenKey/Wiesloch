package com.wiesloch.po;

/**
 * 
 * ClassName:DistributorVo<br/>
 * Description:分销商最终视图类，通过这个类来显示，或者通过它接收信息<br/>
 * E-mail:weijianyututu@163.com <br/>
 * Date:     2016年4月29日 下午4:10:20 <br/>
 * @author   SevenKey
 * @version  1.0
 * @since    JDK 1.7
 *
 */
public class DistributorVo {
	private DistributorExtend distributorExtend;

	public DistributorExtend getDistributorExtend() {
		return distributorExtend;
	}

	public void setDistributorExtend(DistributorExtend distributorExtend) {
		this.distributorExtend = distributorExtend;
	}

}