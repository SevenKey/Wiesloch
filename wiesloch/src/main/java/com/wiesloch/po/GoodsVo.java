package com.wiesloch.po;

/**
 * ClassName:GoodsVo<br/>
 * Description:商品类最终视图类，通过这个类来显示，或者通过它接收信息<br/>
 * E-mail:weijianyututu@163.com <br/>
 * Date: 2016年4月29日 下午4:30:51 <br/>
 * 
 * @author SevenKey
 * @version 1.0
 * @since JDK 1.7
 *
 */
public class GoodsVo {
	private Goods goods;

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

}
