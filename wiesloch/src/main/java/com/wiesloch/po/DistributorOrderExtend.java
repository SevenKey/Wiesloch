package com.wiesloch.po;

/**
 * ClassName:DistributorOrderExtend<br/>
 * Description:分销商订单扩展类，可以加入一些其他的字段信息来扩展我们的分销商订单类<br/>
 * E-mail:weijianyututu@163.com <br/>
 * Date: 2016年4月29日 下午4:18:59 <br/>
 * 
 * @author SevenKey
 * @version 1.0
 * @since JDK 1.7
 *
 */
public class DistributorOrderExtend extends DistributorOrder {

}
