package com.wiesloch.po;

/**
 * ClassName:DistributorOrderDetailVo<br/>
 * Description:分销商订单细节最终视图类，通过这个类来显示，或者通过它接收信息<br/>
 * E-mail:weijianyututu@163.com <br/>
 * Date: 2016年4月29日 下午4:26:01 <br/>
 * 
 * @author SevenKey
 * @version 1.0
 * @since JDK 1.7
 *
 */
public class DistributorOrderDetailVo {
	private DistributorOrderDetail distributorOrderDetail;

	public DistributorOrderDetail getDistributorOrderDetail() {
		return distributorOrderDetail;
	}

	public void setDistributorOrderDetail(DistributorOrderDetail distributorOrderDetail) {
		this.distributorOrderDetail = distributorOrderDetail;
	}

}
